package semangatdaspro;

/**
 *
 * @author Raga
 */
public class No3 {
      public static void main(String[] args) {
          String ab="";
          
          // PESAN RAHASIA YANG HARUS DIPECAHKAN
        String pesanrahasia="-.-..-.-..-.-..--.-..-.--.-.--.--....-..-......--.-.--.--....-.--.--.-.---."
                + "-.-..-......--...-..--..-.-.--.--...---.-.-.--.--.-..-......--.--.-.--..-.-.---..-..--...."
                + "-.--.-..-.--.-.....-......--.-.--.--..-.-.---..--.---.-.-.--.-.--.---..--.--..-.-.---..--.--"
                + "....-.--.---...-.--....-......--.-.-..--....-.--.---..--..---.--....-.--.---...-......---.....--."
                + ".-.-.---..-..--.---..--....-.--.-.....-......--...-..--..-.-.---..-..--.-....--..-.-.--.---..---.-...--"
                + ".-..-..-......---.-.-.--.---..---.-...---.-.-.--.-.--..-......---.-...--..-.-.---..-..---.-.-.---..--..-.."
                + "....--.--.-.--..-.-.--.---..--...--.--.----.--...-..--....-..-....-..--.--."
        ;
      
        // TAMPILAN PESAN RAHASIA YANG BELUM DIPECAHKAN
         System.out.println(pesanrahasia);      

         
        // MENGUBAH PESAN RAHASIA MENJADI BINER
       for (int w=0;w<pesanrahasia.length();w++) {
        String a = pesanrahasia.substring(w, w+1);
            if (a.equalsIgnoreCase(".")){
                ab+="0";
            } else {
                ab+="1";
            }
       }
       
       // MENYAMAKAN KODEBINARY = AB;
       String kodebinary = ab;
       
       
       // PROSES MENGAMBIL, MEMBACA 8 BIT BINER UNTUK MENCARI SUFFIX DAN ASCII
       String carisuffix = kodebinary.substring(ab.length()-8, ab.length()); // MENCARI 8 BIT BINER UNTUK MENCARI SUFFIX
       String suffix = ConvertBinary(carisuffix); // MEMBACA 8 BIT BINER SUFFIX YANG DIDAPATKAN UNTUK DIKONVERSIKAN KE ASCII
       int prefix =  Integer.parseInt(suffix); // HASIL DARI STRING DIPARSING MENJADI INTEGER
       
       String binary = kodebinary.substring(prefix, ab.length()-8); // MEMBACA INDEKS NILAI PREFIX
       
       String kata = binary, huruf = null;
       int b = 0;
       
       // PROSES MEMBAGI MENJADI 8 BIT BINER
       for (int x = 0; x <kata.length()/8; x++) {
           huruf = kata.substring(b,b+8);
           
           String kalimat= ConvertBinary(huruf);
           b=b+8;
           System.out.print(kalimat); // PESAN RAHASIA YANG TELAH DIPECAHKAN
       }
        System.out.print("");
    }
    
    // PROSES KONVERSI BINER KE ASCII
    
    static String ConvertBinary(String binary) {
        StringBuilder sbuilder= new StringBuilder();
        int[] c={1,2,4,8,16,32,64,128};
        for (int y=0;y < binary.length();y+=9) {
            int indeks=0;
            int jumlah=0;
            for (int z= 7; z >=0; z--) {
                if (binary.charAt(z+y)=='1') {
                    jumlah += c[indeks];
                }
                indeks++;
            }
            sbuilder.append(Character.toChars(jumlah));
        }
        return sbuilder.toString();
    }
   
    }


  
