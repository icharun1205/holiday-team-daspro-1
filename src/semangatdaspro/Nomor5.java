/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semangatdaspro;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Icaa
 */
public class Nomor5 {
    public static void main(String[] args) {
        //variable
        String kata, temp;
        int ctr, ctr2;
        int max = 0;
        int min = 99;
        //array untuk tempat character untuk dicek
        //dd untuk array max
        //di untuk array min
        List<Integer> dd = new ArrayList<Integer>();
        List<Integer> di = new ArrayList<Integer>();
        // TODO code application logic here
        //scanner
        Scanner input = new Scanner (System.in);
        System.out.print("Kata : ");
        kata = input.next();
        //Masukan 2 digit anggka dari kata ke dd
        for (int x=0; x<(kata.length()-1); x++){
            if(Character.isDigit(kata.charAt(x)) && Character.isDigit(kata.charAt(x+1))) { //cek per 2 digit
                ctr = (Character.getNumericValue(kata.charAt(x))*10) + Character.getNumericValue(kata.charAt(x+1)); //gabungkan jadi puluhan
                dd.add(ctr); //masukkan ke array max (dd)
            }
        }
        
        //cek per 1 digit
        for (int x=0; x<(kata.length()); x++){
            if(Character.isDigit(kata.charAt(x))) {
                ctr = Character.getNumericValue(kata.charAt(x));
                dd.add(ctr);
            }
        }
        //Cek angka terbesar pada array DD
        for (int x=0; x<dd.size(); x++) {
            if (dd.get(x) > max) {
              max = dd.get(x);  
            }
        }
//        System.out.println("DD : " + dd);
        if (max == 0) {
            System.out.println("Max : -");
        } else {
            System.out.println("Max : " + max);
        }
        //temp merupakan "kata" tanpa 'max'
        temp = kata.replace(String.valueOf(max), "");
//        System.out.println("Trim : " + temp);
        //Masukan anggka kedalam di
        for (int x=0; x<(temp.length()); x++){
            if(Character.isDigit(temp.charAt(x))) {
                ctr2 = Character.getNumericValue(temp.charAt(x));
                di.add(ctr2);
            }
        }
        //Cek bilangan terkecil pada DI
        for (int x=0; x<di.size(); x++) {
            if (di.get(x) < min) {
              min = di.get(x);  
            }
        }
//        System.out.println("DI : " + di);

        //klo gk ada yg bisa di cek 
        if (min == 99) {
            System.out.println("Min : -");
        } else {
            System.out.println("Min : " + min);
        }
    }
}