package semangatdaspro;

/**
 *
 * @author Icaa
 */
public class Nmr6 extends javax.swing.JFrame {

   private String table = "abcdefghijklmnopqrstuvwxyz�����ABCDEFGHIJKLMNOPQRSTUVWXYZ�����1234567890.,;_:+-*/ @$#�?!�=()[]{}\\\'\"";
   
    public Nmr6() { //kunci buat ngebuka desc nya angka 6
        
       initComponents();
        
       setLocationRelativeTo(this);
    }
        private String Clean_text(String text)
     {
        text = text.replaceAll("\n", "");  
         
        for(int x = 0; x < text.length(); x++)
        {
            int position = table.indexOf(text.charAt(x));
            
            if (position == -1)
            {
                text = text.replace(text.charAt(x), ' ');
            }
        }        
        return text;
    }        
    public String Encrypt(String text, int key)
        {       
        String cleaned_text = Clean_text(text);
        
        String encrypted = "";  
        
     for(int i = 0; i < cleaned_text.length();i++)
        {
           int position = table.indexOf(cleaned_text.charAt(i));
           
           if ((position + key) < table.length())
            {
                encrypted += table.charAt(position + key);
            }
            else
            {
                encrypted +=  table.charAt((position + key) - table.length());
            }         
        }        
        return encrypted;
    }
    
    public String Decrypt(String text, int key)
    {        
        String cleaned_text = Clean_text(text);
        
        String decrypted = "";   
        
        for(int x = 0; x < cleaned_text.length(); x++)
        {            
            int position = table.indexOf(cleaned_text.charAt(x)); 
            
            if ((position - key) < 0)
            {
                decrypted += table.charAt((position - key) + table.length());
            }
            else
            {
                decrypted += table.charAt(position - key );
            }         
        }        
        return decrypted;
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        output_area = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        input_area = new javax.swing.JTextArea();
        decrypt_button_ = new javax.swing.JButton();
        encrypt_button_ = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        key_field = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        output_area.setColumns(20);
        output_area.setRows(5);
        jScrollPane1.setViewportView(output_area);

        input_area.setColumns(20);
        input_area.setRows(5);
        jScrollPane2.setViewportView(input_area);

        decrypt_button_.setText("Decrypt");
        decrypt_button_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decrypt_button_ActionPerformed(evt);
            }
        });

        encrypt_button_.setText("Encrypt");
        encrypt_button_.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encrypt_button_ActionPerformed(evt);
            }
        });

        jLabel1.setText("Enter key");

        key_field.setText("6");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(encrypt_button_, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(decrypt_button_, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(93, 93, 93)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(key_field, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(decrypt_button_)
                    .addComponent(encrypt_button_)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(key_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void encrypt_button_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encrypt_button_ActionPerformed
      
         String input = input_area.getText();
        
        int key = Integer.parseInt(key_field.getText());
        
        if (key <= 10)
        {
            output_area.setText(Encrypt(input, key));
        }        
    }//GEN-LAST:event_encrypt_button_ActionPerformed

    
    private void decrypt_button_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decrypt_button_ActionPerformed
      
        String input = input_area.getText();
        
        int key= Integer.parseInt(key_field.getText());
        
        if (key <= 10)
        {
            output_area.setText(Decrypt(input, key ));
        }

    }//GEN-LAST:event_decrypt_button_ActionPerformed

    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            
            public void run() {
                
                new Nmr6().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton decrypt_button_;
    private javax.swing.JButton encrypt_button_;
    private javax.swing.JTextArea input_area;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField key_field;
    private javax.swing.JTextArea output_area;
    // End of variables declaration//GEN-END:variables
}
